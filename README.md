# DATA STRUCTURES AND ALGORITHMS USING JAVA #

This repository contains Python code for various data structures and algorithms 
than are commonly used to solve different problems.

### Pre requisites ###

* Basic knowledge in programming 
* At least Java 8. Run command `java --version` to check python version.
* Your favorite IDE 
* Command line terminal

# CONTENT #